use regex::Regex;
use std::collections::BTreeMap;
use std::ops::Range;
use std::str::FromStr;
const RE_RANGE: &str = r#"^\{([0-9]+)..([0-9]+)}$"#;
#[derive(Debug)]
enum Error {
    InvalidInput(String),
    FormatNotRange,
}

trait Validator {
    fn new(format: &str) -> Result<Self, Error>
    where
        Self: Sized;
    fn validate(&self, input: &str) -> Result<String, Error>;
}

struct UnsupportedValidator {
    format: String,
}

impl Validator for UnsupportedValidator {
    fn new(format: &str) -> Result<Self, Error> {
        Ok(Self {
            format: format.into(),
        })
    }

    fn validate(&self, input: &str) -> Result<String, Error> {
        Ok(input.into())
    }
}

struct RangeValidator {
    format: String,
    range: Range<usize>,
}

impl Validator for RangeValidator {
    /// Input should be {NUM..NUM}
    fn new(format: &str) -> Result<Self, Error> {
        let regex = Regex::new(RE_RANGE).expect("Regex should compile?");
        let caps = regex.captures(format).ok_or(Error::FormatNotRange)?;
        let start = if let Some(Ok(num)) = caps
            .get(1)
            .map(|value| value.as_str())
            .map(|value| value.parse::<usize>())
        {
            Some(num)
        } else {
            None
        };
        let end = if let Some(Ok(num)) = caps
            .get(2)
            .map(|value| value.as_str())
            .map(|value| value.parse::<usize>())
        {
            Some(num + 1)
        } else {
            None
        };
        let range = if let (Some(start), Some(end)) = (start, end) {
            Range { start, end }
        } else {
            return Err(Error::FormatNotRange);
        };

        Ok(Self {
            format: format.to_string(),
            range,
        })
    }

    fn validate(&self, input: &str) -> Result<String, Error> {
        let value: usize = input
            .parse()
            .map_err(|_| Error::InvalidInput("Input is not number".into()))?;
        if !self.range.contains(&value) {
            return Err(Error::InvalidInput(format!(
                "Value is not in the range: {}",
                self.format
            )));
        }
        Ok(input.into())
    }
}

struct Recipe {
    input_format: Box<dyn Validator>,
}

struct Foo<'a> {
    input: &'a str,
    validator: Box<dyn Validator>,
}

impl<'a> Foo<'a> {
    fn new<T>(input: &'a str, validator: T) -> Self
    where
        T: Validator + 'static,
    {
        Self {
            input,
            validator: Box::new(validator),
        }
    }
}

fn parse<'a, T>(input: &'a str) -> Result<T, Error>
where
    T: FromStr,
{
    T::from_str(input).map_err(|e| Error::InvalidInput(String::default()))
}

type Parser<T: FromStr> = Box<dyn Fn(&str) -> Result<T, Error>>;
struct Lala<'a, T>(&'a str, Parser<T>);

/*
impl<'a, T> Lala<'a, T> {
    fn new(input: &'a str, parse: Parser<T>) -> Self {
        Self { input, parse }
    }
}
        */

fn main() {
    let a = vec![Foo::new("foo", RangeValidator::new("{1..2}").unwrap())];
    let b: Vec<(String, Parser<_>)> = vec![
        (String::new("usize"), Box::new(parse::<usize>)),
        (String::new("isize"), Box::new(parse::<isize>)),
    ];

    let mut map = BTreeMap::new();

    let range = Box::new(RangeValidator::new("{1..2}").unwrap());
    let range2 = Box::new(RangeValidator::new("{1..20}").unwrap());
    let unsupported = Box::new(UnsupportedValidator::new("some wierd parslet").unwrap());
    map.insert(
        String::from("foo.bar".to_string()),
        Recipe {
            input_format: range,
        },
    );
    map.insert(
        String::from("foo.lol".to_string()),
        Recipe {
            input_format: range2,
        },
    );
    map.insert(
        String::from("unsupported".to_string()),
        Recipe {
            input_format: unsupported,
        },
    );
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_range_validator() {
        assert!(RangeValidator::new("{10..209}").is_ok());
        assert!(RangeValidator::new("{1}").is_err());
        assert!(RangeValidator::new("GARBAGE").is_err());
    }
    #[test]
    fn test_ranges() {
        let range = RangeValidator::new("{1..2}").unwrap();
        assert!(range.parse("1").is_ok());
        assert!(range.parse("2").is_ok());
        assert!(range.parse("0").is_err());
        assert!(range.parse("3").is_err());
    }
}
